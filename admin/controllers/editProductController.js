var Item = require('../models/items');
const {ObjectId} = require('mongodb');

exports.editProduct = function(req, res) {
    Item.find({_id: ObjectId(req.query.id)})
      .exec(function (err, items) {
        if (err) { return next(err); }
        var item = items[0]
        res.render('./creatProduct/admin-panel-page-products-edit', { title: 'Cập nhật', item: item});
      });
};