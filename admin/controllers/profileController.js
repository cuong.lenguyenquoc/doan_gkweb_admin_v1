var Admin = require('../models/admin');
const {ObjectId} = require('mongodb');

exports.profile = function(req, res) {
    Admin.findOne({_id: ObjectId(req.session.passport.user)})
      .exec(function (err, admin) {
        if (err) { return next(err); }
        let isTap1Active = true
        dataForm = {
          name : admin.information.name,
          address : admin.information.address,
          website : admin.information.website,
          phoneNumber: admin.information.phoneNumber
        }
        var messages = req.flash('statusUpdate');
        var dataFormArray = req.flash('dataForm');
        if (dataFormArray.length > 0) {
          dataForm = dataFormArray[0]
          isTap1Active = false
        }
        res.render('./profile/admin-panel-page-profile', { 
          admin: admin,
          messages: messages,
          hasErrors: messages.length > 0,
          dataForm : dataForm,
          isTap1Active: isTap1Active
         });
      })
};

exports.checkUpdatedData = function(req, res, next) {
  // form values
  var name = req.body.name;
  var phoneNumber = req.body.phoneNumber
  var address = req.body.address;
  var website = req.body.website
  //check form validation
  req.checkBody("name", "name is required").notEmpty();
  req.checkBody("phoneNumber", "Vui lòng điền số điện thoại đúng định dạng").isMobilePhone()
  req.checkBody("address", "address is required").notEmpty();
  req.checkBody("website", "website is required").notEmpty
  //check for errors
  var errors = req.validationErrors();
  dataForm = {
    name : name,
    address : address,
    website : website,
    phoneNumber: phoneNumber
  }
  if (errors) {
      var messages = [];
      errors.forEach(function(error){
          messages.push(error.msg);
      });
      req.flash('statusUpdate', messages)
      req.flash('dataForm', dataForm)
      res.redirect('/profile')
  } else {
      next()
  }
}

exports.update = function(req, res, next) {
  Admin.findOne({_id: ObjectId(req.session.passport.user)})
    .exec(function (err, admin) {
      if (err) { return next(err); }
      admin.information.name = req.body.name
      admin.information.phoneNumber = req.body.phoneNumber
      admin.information.address = req.body.address
      admin.information.website = req.body.website
      admin.save();
      res.redirect('/profile')
    });
};