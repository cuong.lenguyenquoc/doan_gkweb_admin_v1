exports.login = function(req, res) {
  var messages = req.flash('error');
    var dataFormArray = req.flash('dataForm');
  dataForm = {
              email : '',
              password : ''
  }
  if (dataFormArray.length > 0) {
    dataForm.email = dataFormArray[0].email
    dataForm.password = dataFormArray[0].password
  }
  res.render('./login/login',{
      messages: messages,
      hasErrors: messages.length > 0,
      dataForm : dataForm,
  });
};