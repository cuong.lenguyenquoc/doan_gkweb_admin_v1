var mongoose = require('mongoose');
var bcrypt   = require('bcryptjs');

var AdminSchema = mongoose.Schema({
    email: String,
    password: String,
    information: {
       name: String,
       phoneNumber: String,
       address: String,
       website: String
    }
});


AdminSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

AdminSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

module.exports = mongoose.model('Admin', AdminSchema);
