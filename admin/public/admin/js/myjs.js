import { render } from "pug";
var Item = require('../models/items');

var curday = function(sp){
	today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //As January is 0.
	var yyyy = today.getFullYear();
	
	if(dd<10) dd='0'+dd;
	if(mm<10) mm='0'+mm;
	return (mm+sp+dd+sp+yyyy);
	};
  
  
function ItemCreate(name, type, old_price, new_price, import_price, created_at, updated_at, description, image, cb) {
		item_detail = { 
			name: name,
			old_price: old_price,
			new_price: new_price,
			import_price: import_price,
			created_at: created_at,
			updated_at: updated_at,
			description: description,
			image: image
		}
		if (type != false) item_detail.type = type
			
		var NewItem = new Item(item_detail);    
		NewItem.save(function (err) {
			if (err) {
			cb(err, null)
			return
			}
			cb(null, NewItem)
		}  );
}







