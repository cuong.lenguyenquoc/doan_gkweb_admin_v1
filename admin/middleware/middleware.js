
exports.requiresLogin = function requiresLogin(req, res, next) {
    if (req.session && req.session.passport && req.session.passport.user) {
      return next();
    } else {
      var err = new Error('You must be logged in to view this page.');
      err.status = 401;
      res.redirect('/login')
    }
  }