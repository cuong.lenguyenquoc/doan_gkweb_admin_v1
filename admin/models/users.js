var mongoose = require('mongoose');

var UserSchema = mongoose.Schema({
    email: String,
    password: String,
    isBlocked: Boolean,
    information: {
       name: String,
       phoneNumber: String
    }
});

module.exports = mongoose.model('User', UserSchema);
