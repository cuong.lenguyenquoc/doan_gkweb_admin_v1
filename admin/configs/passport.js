var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var Admin = require('../models/admin');

passport.serializeUser(function(admin, done) {
    done(null, admin.id);
  });
  
passport.deserializeUser(function(id, done) {
    Admin.findById(id, function(err, admin) {
      done(err, admin);
    });
  });

/* Passport login */
passport.use('local-login', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
}, function(req, email, password, done) {
    Admin.findOne({
        'email': email
    }, function(err, admin) {
        if (err) {
            return done(err);
        }
        if (!admin) {
            req.flash('dataForm',dataForm)
            return done(null, false, {
                message: 'Tài khoản này không tồn tại, vui lòng kiểm tra lại.'
            });
        }
        if (!admin.validPassword(password)) {
            req.flash('dataForm',dataForm)
            return done(null, false, {
                message: 'Mật khẩu không đúng, vui lòng kiểm tra lại.'
            });
        }
        return done(null, admin);
    });
}));