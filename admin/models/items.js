var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var ItemSchema = new Schema(
  {
    name: {type: String, required: true, max: 100},
    type: {type: String, required: true, enum: ['mensJacket', 'mensJeans', 'mensShirt', 'mensShort', 'mensTrousers', 'mensTshirt', 'mensWallet', 'mensWatch', 'womensJacket', 'womensJeans', 'womensShirt', 'womensShort', 'womensTrousers', 'womensTshirt', 'womensWallet', 'womensWatch'], default: 'womensJacket'},
    old_price: {type: Number, required: true},
    new_price: {type: Number, required: true},
    description: {type: String},
    number: {type: Number, required: true},
    image: {type:String, required: true},
    import_price: {type: Number, required: true},
    created_at: {type: String, required: true},
    updated_at: {type: String, required: true}
  }
);

// Phương thức ảo cho URL
ItemSchema
.virtual('url')
.get(function () {
  return '/edit-product?id=' + this._id;
});

ItemSchema
.virtual('category')
.get(function () {
    switch (this.type) {
        case 'mensJacket':
            return 'Áo khoác nam'
        case 'mensJeans':
            return 'Quần Jeans nam'
        case 'mensShirt':
            return 'Áo sơ mi nam'
        case 'mensShort':
            return 'Quần sort nam'
        case 'mensTrousers':
            return 'Quần tây nam'
        case 'mensTshirt':
            return 'Áo thun nam'
        case 'mensWallet':
            return 'Ví nam'
        case 'mensWatch':
            return 'Đông hồ nam'
        case 'womensJacket':
            return 'Áo khoác nữ'
        case 'womensJeans':
            return 'Quần Jeans nữ'
        case 'womensShirt':
            return 'Áo sơ mi nữ'
        case 'womensShort':
            return 'Quần sort nữ'
        case 'womensTrousers':
            return 'Quần tây nữ'
        case 'womensTshirt':
            return 'Áo thun nữ'
        case 'womensWallet':
            return 'Ví nữ'
        case 'womensWatch':
            return 'Đông hồ nữ'
        default:
            return 'Không xác định'
    }
});

//xuất mô hình
module.exports = mongoose.model('Item', ItemSchema);