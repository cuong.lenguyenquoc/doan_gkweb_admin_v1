var express = require('express');
var router = express.Router();

// Require controller modules.
var statistical_controller = require('../controllers/statisticalController');
var user_profile_controller = require('../controllers/userProfileController');
var create_product_controller = require('../controllers/createProductController');
var product_controller =  require('../controllers/productController');
var profile_controller = require('../controllers/profileController');
var user_controller = require('../controllers/userController');
var item_controller = require('../controllers/itemController');
var edit_product_controller = require('../controllers/editProductController');
var login_controller = require('../controllers/loginController')
var logout_controller = require('../controllers/logoutController')
var middleware = require('../middleware/middleware')
var passport = require('passport');

// GET catalog home page.
router.get('/', middleware.requiresLogin, statistical_controller.index);

router.get('/user-profile', middleware.requiresLogin, user_profile_controller.userProfile);

router.get('/create-product', middleware.requiresLogin, create_product_controller.creatProduct);

router.get('/product', middleware.requiresLogin, product_controller.product);

router.get('/profile', middleware.requiresLogin, profile_controller.profile);

router.post('/profile', middleware.requiresLogin, profile_controller.checkUpdatedData, profile_controller.update)

router.get('/user', middleware.requiresLogin, user_controller.user);

router.get('/login', login_controller.login);

router.route('/login').post(function (req, res, next) {
    // form values
    var email = req.body.email;
    var password = req.body.password;
    //check form validation
    req.checkBody("email", "email is invalid").isEmail();
    req.checkBody("password", "password is required").notEmpty();
    //check for errors
    var errors = req.validationErrors();
  
    dataForm = {
        email : email,
        password : password
    }
    if (errors) {
        var messages = [];
        errors.forEach(function(error){
            messages.push(error.msg);
        });
        res.render('login/login',{
            messages: messages,
            hasErrors: messages.length > 0,
            dataForm : dataForm
        });
    } else {
       next()
    }
  }, passport.authenticate('local-login',{
        successRedirect:'/',
        failureRedirect:'/login',
        failureFlash : true  
  }))

router.get('/logout', logout_controller.logout);

router.post("/add-product", middleware.requiresLogin,item_controller.addProduct);

router.get('/edit-product', middleware.requiresLogin, edit_product_controller.editProduct) ;

router.post('/update-product', middleware.requiresLogin, item_controller.updateProduct) ;

router.post('/delete-product', middleware.requiresLogin, item_controller.deleteProduct);

router.post('/block-user', middleware.requiresLogin, user_controller.block)

module.exports = router;
