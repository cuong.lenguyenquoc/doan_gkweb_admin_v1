var Item = require('../models/users');
const {ObjectId} = require('mongodb');

exports.user = function(req, res) {
    Item.find()
      .exec(function (err, list_items) {
        if (err) { return next(err); }
        //Successful, so render
        let pages =parseInt( list_items.length / 10 )
        if(list_items.length % 10 > 0){
          pages = pages + 1
        }
        var page = req.query.page
        if (page > pages) {
            page = pages
        } else if (page < 1) {
            page = 1
        }
        var i = 10 * (page - 1) 
        var n 
        if(pages == page){
          if (list_items.length % 10 == 0) {
            n = 10 * (page-1) + list_items.length % 10  + 1
          } else {
            n = 10 * (page-1) + list_items.length % 10 
          }
        
      }else{ 
        n= 10 * page
      } 
      console.log(i, n)

    res.render('./user/admin-panel-page-users', { title: 'Express',pages: pages, list_items: list_items, page: page, i:i, n:n });
});
};

exports.block = function(req, res) {
    Item.find({_id: ObjectId(req.query.id)})
    .exec(function (err, items) {
      if (err) { return next(err); }
      var item = items[0]
      item.id = req.params.id
      if(item.isBlocked == false){
          item.isBlocked = true
      }else{
          item.isBlocked = false
      }
      Item.findByIdAndUpdate(req.query.id, item, {}, function (err,item) {
        if (err) { return next(err); }
           // Successful - redirect to book detail page.
           res.redirect('user?page='+req.query.page);
        });
    });
}