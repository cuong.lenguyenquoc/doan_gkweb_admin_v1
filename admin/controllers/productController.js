var Item = require('../models/items');

exports.product = function(req, res) {
    Item.find()
      .exec(function (err, list_items) {
        if (err) { return next(err); }
        //Successful, so render
        let pages =parseInt( list_items.length / 15 )
        if(list_items.length % 15 > 0){
          pages = pages + 1
        }
        var page = req.query.page
        if (page > pages) {
            page = pages
        } else if (page < 1) {
            page = 1
        }
        var i = 15 * (page - 1) 
        var n 
        if(pages == page){
        n = 15 * (page-1) + list_items.length % 15} 
        else{ 
        n= 15 * page
      } 
      res.render('./product/admin-panel-page-products', { title: 'Express', pages: pages, list_items: list_items, page: page, i:i, n:n });
      });
};