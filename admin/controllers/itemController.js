var Item = require('../models/items');
const {ObjectId} = require('mongodb');

var curday = function(sp){
	today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //As January is 0.
	var yyyy = today.getFullYear();
	
	if(dd<10) dd='0'+dd;
	if(mm<10) mm='0'+mm;
	return (mm+sp+dd+sp+yyyy);
	};

exports.addProduct = function(req, res) {
    var item = new Item( { name: req.body.product_name, type: req.body.product_category, old_price: req.body.old_price,
        new_price:req.body.new_price,description: req.body.product_details,import_price: req.body.import_price, number:req.body.number
    ,created_at:curday("-"),updated_at:curday("-"),image:req.body.image} );
    item.save(function (err) {
        if (err) { return next(err); }
            //successful - redirect to new book record.
            res.redirect(item.url);
        });
};

exports.updateProduct = function(req, res) {
    Item.find({_id: ObjectId(req.query.id)})
    .exec(function (err, items) {
      if (err) { return next(err); }
      var item = items[0]
      item.id = req.params.id
      item.name = req.body.product_name
      item.type = req.body.product_category
      item.old_price = req.body.old_price
      item.new_price = req.body.new_price
      item.description = req.body.product_details
      item.import_price = req.body.import_price
      item.image = req.body.image
      item.number = req.body.number
      item.updated_at = curday("-")
      item.created_at = items[0].created_at
      Item.findByIdAndUpdate(req.query.id, item, {}, function (err,item) {
        if (err) { return next(err); }
           // Successful - redirect to book detail page.
           res.redirect(item.url);
        });
    });
}

exports.deleteProduct = function(req, res) {
    Item.find({_id: ObjectId(req.query.id)})
    .exec(function (err, items) {
      if (err) { return next(err); }
      Item.findByIdAndRemove(req.query.id, function (err,item) {
        if (err) { return next(err); }
           // Successful - redirect to book detail page.
           res.redirect('/product?page='+req.query.page);
        });
    });
    
}
