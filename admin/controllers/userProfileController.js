var Item = require('../models/users');
const {ObjectId} = require('mongodb');

exports.userProfile = function(req, res) {
    Item.find({_id: ObjectId(req.query.id)})
      .exec(function (err, items) {
        if (err) { return next(err); }
        var item = items[0]
        res.render('./userProfile/admin-panel-page-users-profile', { title: 'Express', item: item});
      });
};